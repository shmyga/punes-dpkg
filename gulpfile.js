const gulp = require('gulp');
const deb = require('gulp-deb');
const version = require('./package.json').version;

const arch = 'amd64';

gulp.task('build', function () {

    return gulp
        .src([
            'src/**',
        ], { base: 'src' })
        .pipe(deb(`punes_${version}_${arch}.deb`, {
            name: 'puNES',
            version: version,
            maintainer: {
                name: 'Shmyga',
                email: 'shmyga.z@gmail.com'
            },
            architecture: arch,     // Optional. String. Architecture
            installedSize: 0,     // Optional. Integer. Installed-Size, KiB.
            preDepends: [],                       // Optional. Array or String. Pre-Depends
            depends: ['libqtgui4', 'libcg', 'libcggl'],                       // Optional. Array or String. Depends
            recommends: null,        // Optional. Array or String. Recommends
            suggests: null,          // Optional. Array or String. Suggests
            enhances: null,          // Optional. Array or String. Enhances
            section: 'game',        // Optional. String. Section
            priority: 'optional',    // Optional. String. Priority
            homepage: 'https://github.com/punesemu/puNES', // Optional. String. Homepage
            short_description: 'puNES',
            long_description: 'Nintendo Entertaiment System emulator',
            postinst: [
                'if [ "$1" = "configure" ] && [ -x "`which update-menus 2>/dev/null`" ] ; then\n' +
                '    update-menus\n' +
                'fi'
            ],
        }))
        .pipe(gulp.dest('builds/'));
});
